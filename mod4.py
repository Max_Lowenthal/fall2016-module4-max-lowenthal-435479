#!/usr/bin/env python
import operator, re, sys, os
if len(sys.argv) < 2:
		sys.exit("No File is present")

filename = sys.argv[1]

if not os.path.exists(filename):
	sys.exit("The file you input is not here")

f = open(filename)

regex_compile = re.compile("(?P<names>[A-Z]\w*\s[A-Z]\w*)\s\w*\s(?P<bats>\d)\s\w*\s\w*\s(?P<hits>\d)")

batsDict = {}
hitsDict = {}
batAvg = {}

for line in f:
        if re.search('(hits)', line):
                find_n = re.match(regex_compile, line).group('names')
                if find_n in hitsDict:
                        batsDict[find_n] = batsDict[find_n] + float(re.match(regex_compile, line).group('bats'))
                        hitsDict[find_n] = hitsDict[find_n] + float(re.match(regex_compile, line).group('hits'))
                else:
                        batsDict[find_n] = float(re.match(regex_compile, line).group('bats'))
                        hitsDict[find_n] = float(re.match(regex_compile, line).group('hits'))
                        

for find_n in batsDict:
        batAvg[find_n] = round(float(hitsDict[find_n]/batsDict[find_n]), 3)  

sort_avg = sorted(batAvg.items(), key=operator.itemgetter(1))        
for i in reversed(sort_avg):
        if len(str(i[1])) <= 4:
                print str(i[0]) + ": " + str(i[1]) + "0"
        else: print str(i[0]) + ": " + str(i[1])


